# XKSS-个人博客

## 预览地址

[https://zjs123456789.gitee.io/xkss](https://zjs123456789.gitee.io/xkss "预览地址")。

### 介绍 :trophy:

个人博客网站，基于 less+rem+媒体查询实现响应式布局，同时适配了<strong>移动端</strong>

### 技术栈

HTMl+CSS+LESS+REM+媒体查询

### 使用说明

直接在浏览器运行 index.html 文件就行
